import 'react-native-gesture-handler';
import React from 'react';



//import screen

import Home from './src/Screens/Home/Home'
import Create from './src/Screens/Create/Create';
import Detail from './src/Screens/Detail/Detail';

//import components
import { navigationRef } from './src/Functions/Nav'

// import from redux


//import navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();

export default function Root() {


    return (
        <>

            <NavigationContainer ref={navigationRef}>
                <Stack.Navigator
                    //
                    initialRouteName={"Home"}
                >
                    <Stack.Screen
                        options={{ headerShown: true, headerLeft: null }}
                        name="Home"
                        component={Home}
                    />
                    <Stack.Screen
                        options={{ headerShown: true }}
                        name="Create"
                        component={Create}
                    />
                    <Stack.Screen
                        options={{ headerShown: true }}
                        name="Detail"
                        component={Detail}
                    />

                </Stack.Navigator>
            </NavigationContainer>

        </>
    );
}





