import React from 'react'
import { CheckBox, StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-elements'
import { widthPercentageToDP } from 'react-native-responsive-screen'
import { moderateScale } from 'react-native-size-matters'


export default function Tombol(props) {

    const styles = StyleSheet.create({
        textStyle: {
            color: props.textColor ? props.textColor : 'white',
            fontFamily: 'Quicksand-SemiBold',
        },

        container: {
            backgroundColor: props.backgroundColor ? props.backgroundColor : '#C78EF7',
            borderRadius: widthPercentageToDP(28.5),
            height: moderateScale(50),

        },
        shadow: {
            elevation: 5,
            width: widthPercentageToDP(90),
            borderRadius: widthPercentageToDP(50),

        }


    })

    return (
        <Button
            title={props.judul}
            titleStyle={styles.textStyle}
            buttonStyle={styles.container}
            containerStyle={props.style ? [styles.shadow, props.style] : styles.shadow}
            onPress={props.onPress}
            disabled={props.disabled}
            activeOpacity={0}

        />
    )
}

const styles = StyleSheet.create({
    textStyle: {
        color: 'white',
        fontFamily: 'Quicksand-SemiBold',
    },

    container: {
        backgroundColor: '#AB6FDC',
        borderRadius: widthPercentageToDP(28.5),
        height: moderateScale(50),

    },
    shadow: {
        elevation: 5,
        width: widthPercentageToDP(90),
        borderRadius: widthPercentageToDP(50),

    }
}
)


