import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import Feather from 'react-native-vector-icons/Feather';
import Icon from "react-native-vector-icons/Feather"






export function EmailInput(props) {

    return (
        <View style={styles.container0}>
            <TextInput
                errorMessage={props.errorMessage}
                keyboardType={props.keyboardType}
                onBlur={props.onBlur}
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                value={props.value}
                style={[styles.normal, styles.singleline]}
                secureTextEntry={props.secureTextEntry}
                placeholderTextColor="#898B8F"
            />
        </View >
    );
}

export function NameInput(props) {

    return (
        <View style={styles.container0}>
            <TextInput
                onBlur={props.onBlur}
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                value={props.value}
                style={[styles.normal, styles.singleline]}
                secureTextEntry={props.secureTextEntry}
                placeholderTextColor="#898B8F"

            />
        </View >
    );
}

//Basic Input
export function BasicInput(props) {
    const [onChange, setonChange] = useState(false)
    return (
        <View>
            {(props.defaultValue !== undefined || onChange) ? (<View style={{ position: "absolute", top: moderateScale(14), left: moderateScale(20), backgroundColor: "white", padding: moderateScale(2), zIndex: 100 }}>


            </View>) : (<></>)}
            <View style={styles.inputmargin}>
                <TextInput
                    placeholder={props.placeholder}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    style={[styles.normal]}
                    multiline={props.multiline}
                    onChangeText={
                        props.onChangeText
                    }
                    onFocus={() =>
                        setonChange(true)}
                    defaultValue={props.defaultValue}
                />
            </View >
        </View>
    );
}

//Category Input
export function CategoryInput(props) {
    return (
        <View>
            {(props.defaultValue !== undefined || props.value != '') ? (<View style={{ position: "absolute", top: moderateScale(5), left: moderateScale(20), backgroundColor: "white", padding: moderateScale(2), zIndex: 100 }}>


            </View>) : (<></>)}

            <TouchableOpacity
                onPress={props.onPress} activeOpacity={0.8} style={[styles.container, { flexDirection: "row", justifyContent: "space-between" }]}>
                <TextInput
                    placeholder={props.placeholder}
                    editable={false}
                    value={props.value}
                    style={styles.category}


                    defaultValue={props.defaultValue}
                ></TextInput>
                <Icon name="chevron-down" size={moderateScale(20)} color="grey" />
            </TouchableOpacity >
        </View>
    );
}

//TExtArea
export function MultilineInput(props) {



    const [onChange, setonChange] = useState(false)


    return (
        <View>
            {(props.defaultValue !== undefined || onChange) ? (<View style={{ position: "absolute", top: moderateScale(16), left: moderateScale(20), backgroundColor: "white", padding: moderateScale(2), zIndex: 100 }}>


            </View>) : (<></>)}

            <View style={styles.container0}>
                <TextInput
                    placeholder={props.placeholder}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    style={[styles.normal, styles.multiline]}
                    multiline={true}
                    defaultValue={props.defaultValue}

                    onFocus={() =>
                        setonChange(true)}
                />
            </View >
        </View>
    );
}

export function PasswordInput(props) {
    const [passwordVisibility, setPasswordVisibility] = useState(true);
    return (
        <View style={styles.container}>
            <TextInput
                errorMessage={props.errorMessage}
                onBlur={props.onBlur}
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                value={props.value}
                style={styles.password}
                secureTextEntry={passwordVisibility}
                placeholderTextColor="#898B8F"
                color="#3E3E3E"
            />
            <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => {
                    setPasswordVisibility(!passwordVisibility);
                }}>
                <Feather
                    name={passwordVisibility === true ? 'eye-off' : 'eye'}
                    size={moderateScale(22)}
                    color="#9c9c9c"
                />
            </TouchableOpacity>
        </View>
    );
}

export function PasswordInput2(props) {
    const [passwordVisibility, setPasswordVisibility] = useState(true);
    return (
        <View style={styles.container2}>
            <TextInput
                placeholder={props.placeholder}
                onChangeText={props.onChangeText}
                value={props.value}
                style={styles.password}
                secureTextEntry={passwordVisibility}
                placeholderTextColor="#898B8F"
                color="#3E3E3E"
            />
            <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => {
                    setPasswordVisibility(!passwordVisibility);
                }}>
                <Feather
                    name={passwordVisibility === true ? 'eye-off' : 'eye'}
                    size={moderateScale(22)}
                    color="#9c9c9c"
                />
            </TouchableOpacity>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        height: moderateScale(57),
        borderWidth: 1,
        borderRadius: moderateScale(26),
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: moderateScale(19),
        marginTop: moderateScale(20),
        marginBottom: moderateScale(10),
        borderColor: "#d1d2d4",
    },
    container2: {
        height: moderateScale(57),
        borderWidth: 1,
        borderRadius: moderateScale(10),
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: moderateScale(19),
        marginTop: moderateScale(5),
        marginBottom: moderateScale(5),
        borderColor: "#d1d2d4",
    },
    normal: {
        fontSize: moderateScale(16),
        borderWidth: moderateScale(1),
        borderRadius: moderateScale(26),
        paddingHorizontal: moderateScale(19),
        paddingVertical: moderateScale(14),
        borderColor: "#d1d2d4",
        fontFamily: "Quicksand-Regular",
        color: "#3E3E3E"
    },
    password: {
        fontSize: moderateScale(16),
        flex: 1,
        fontFamily: "Quicksand-Regular"
    },
    category: {
        fontSize: moderateScale(16),
        flex: 1,
        fontFamily: "Quicksand-Regular",
        color: "#3E3E3E"
    },
    container0: {
        borderColor: "grey",
    },
    inputmargin: {
        flex: 1,
        marginTop: moderateScale(27),
    },
    singleline: {


        height: moderateScale(52),
    },
    multiline: {

        height: moderateScale(169),
        marginTop: moderateScale(30),
        textAlignVertical: "top",
        fontFamily: "Quicksand-Regular"
    }



});
