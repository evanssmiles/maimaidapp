import { SET_LOADING, SET_SCREEN_MODE } from './GlobalAction';

const initialState = {
    loading: false,
    Success: false,
    isLogged: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING":
            return {
                ...state,
                loading: action.payload,
            };
        case "SET_SUCCESS":
            return {
                ...state,
                Success: action.payload,
            };

        case "SET_IS_LOGGED":
            return {
                ...state,
                isLogged: action.payload,
            };
        default:
            return state;
    }
};
