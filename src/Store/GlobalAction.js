export const actionLoading = payload => {
    return {
        type: "SET_LOADING",
        payload,
    }
}

export const actionSuccess = payload => {
    return {
        type: "SET_SUCCESS",
        payload,
    }
}

export const actionLogged = payload => {
    return {
        type: "SET_IS_LOGGED",
        payload,
    }
}




// 