import { all } from 'redux-saga/effects'

//screen saga
import sagaHome from '../Screens/Home/Redux/sagaHome'
import sagaDetail from '../Screens/Detail/Redux/sagaDetail'
import sagaCreate from '../Screens/Create/Redux/sagaCreate'




function* SagaWatcher() {
        yield all([
                sagaHome(),
                sagaDetail(),
                sagaCreate(),
        ])
}

export default SagaWatcher;