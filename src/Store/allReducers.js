import { combineReducers } from 'redux';

//reducers
import GlobalReducer from './GlobalReducer';
import HomeReducer from '../Screens/Home/Redux/reducer'
import DetailReducer from '../Screens/Detail/Redux/reducer'

export const allReducers = combineReducers({
    Global: GlobalReducer,
    HomeReducer,
    DetailReducer,
});
