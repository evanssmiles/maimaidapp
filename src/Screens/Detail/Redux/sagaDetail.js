import axios from 'axios';
import { takeLatest, put, select } from 'redux-saga/effects';
import { ToastAndroid } from 'react-native'
import { actionLoading } from '../../../Store/GlobalAction'

//import action
import { setDetail } from './action';
import { POST_DETAIL, UPDATE_DETAIL } from "./action";


function* sagaPostDetail(action) {
    try {
        yield put(actionLoading(true))
        const result = yield axios.post(
            `http://maimaid.id:8002/user/view`, { id: `${action.payload}` }
        );

        console.log(result, "ini result read detail");
        console.log(action, "ini action")
        if (result.status === 200) {
            // yield put(setUsernameToReducer(result.data));
            yield put(setDetail(result.data))
            // ToastAndroid.show('Data found', ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }

    } catch (error) {
        console.log(error.Error);
    } finally {
        yield put(actionLoading(false))
    }
}

function* sagaUpdateDetail(action) {
    try {
        yield put(actionLoading(true))
        const result = yield axios.post(
            `http://maimaid.id:8002/user/update`,
            action.payload,
        );

        console.log(result, "ini result update detail");
        console.log(action, "ini action detail")
        if (result.status === 200) {
            // yield put(setUsernameToReducer(result.data));
            // yield put(setDetail(result.data))
            ToastAndroid.show('Data berhasil diubah', ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }

    } catch (error) {
        console.log(error.Error);
        ToastAndroid.show('Data gagal diubah periksa kembali inputan anda', ToastAndroid.LONG, ToastAndroid.BOTTOM);
    } finally {
        yield put(actionLoading(false))
    }
}






function* sagaDetail() {
    yield takeLatest(POST_DETAIL, sagaPostDetail)
    yield takeLatest(UPDATE_DETAIL, sagaUpdateDetail)
}

export default sagaDetail;
