export const SET_DETAIL = 'SET_DETAIL'
export const POST_DETAIL = "POST_DETAIL"
export const UPDATE_DETAIL = "UPDATE_DETAIL"

export const setDetail = payload => {
    return {
        type: SET_DETAIL,
        payload,
    };
};


export const postDetail = payload => {
    return {
        type: POST_DETAIL,
        payload,
    }
}

export const updateDetail = payload => {
    return {
        type: UPDATE_DETAIL,
        payload,
    }
}