import { SET_DETAIL } from "./action";

const initialState = {
    data: [],

};

const DetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_DETAIL:
            return {
                ...state,
                data: action.payload,
            };

        default:
            return state;
    }
};

export default DetailReducer;
