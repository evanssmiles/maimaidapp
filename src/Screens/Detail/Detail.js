import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, ScrollView, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import {
    heightPercentageToDP,
    widthPercentageToDP,
} from 'react-native-responsive-screen';
import CheckBox from '@react-native-community/checkbox'
import { RadioButton } from 'react-native-paper';
import DatePicker from 'react-native-date-picker'
import moment from 'moment'
import md5 from 'md5';
import { Input } from 'react-native-elements'
//import components
import { EmailInput, PasswordInput } from '../../Components/TextInput'
import Tombol from '../../Components/SubmitButton'
import Feather from 'react-native-vector-icons/Feather'
//import redux
import { useDispatch, useSelector } from 'react-redux';
import { postDetail, updateDetail } from './Redux/action'
import { TextInput } from 'react-native';

export default function Detail(props, route) {

    //selector
    const detailData = useSelector(state => state.DetailReducer?.data)
    const id = props.route.params.id
    //const

    const dispatch = useDispatch();
    const [passwordVisibility, setPasswordVisibility] = useState(true);
    const [repeatPasswordVisibility, setRepeatPasswordVisibility] = useState(true);
    const [toggleCheckBox, setToggleCheckBox] = useState(false);
    const [fullname, setFullname] = useState('');
    const [errorFullname, setErrorFullname] = useState(false);
    const [email, setEmail] = useState('');
    const [errorEmail, setErrorEmail] = useState(false);
    const [password, setPassword] = useState('');
    const [errorPassword, setErrorPassword] = useState(false);
    const [errorPassword1, setErrorPassword1] = useState(false);
    const [repeatPassword, setRepeatPassword] = useState('');
    const [gender, setGender] = useState();
    const [dob, setDob] = useState(new Date())

    console.log(detailData, "ini detail data kamu")
    //input reformat
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    // const letters = /^[A-Za-z]+$/;
    const letters = /^[a-zA-Z ]*$/
    const letterNumber = /^[0-9a-zA-Z]+$/;


    useEffect(() => {
        dispatch(postDetail(id))
    }, [])

    //function
    const handleUpdate = () => {
        dispatch(
            updateDetail({
                id,
                fullname,
                email,
                gender,
                password: md5(password),
                dob: moment(dob).format("YYYY-MM-DD"),
            }),
        );
        dispatch(postDetail(id))
        // props.navigation.navigate("Home")
    };
    return (
        <ScrollView style={styles.mainContainer}>
            <View style={styles.blueContainer}>
                {/* blue container */}
                <View style={styles.titleStyle}>
                    <Text style={{ fontFamily: "Quicksand-Bold", color: "white", fontSize: moderateScale(24) }}>Detail Profile</Text>
                </View>
            </View>
            {detailData === undefined || detailData.length < 1 ?
                null :
                <>
                    <View style={styles.containContainer}>
                        {/* isi your progress */}
                        <Text style={{ fontFamily: "Quicksand-Regular", fontSize: moderateScale(16), color: "#898B8F" }}></Text>
                    </View>
                    <View style={styles.input}>
                        <Text style={{ marginBottom: 20 }}>Nama anda (minimal 3 huruf)</Text>
                        <Input
                            errorMessage={errorFullname ? "Hanya boleh huruf dan minimal 3 huruf" : null}
                            onBlur={() => {
                                if (fullname.length < 3 || (letters.test(fullname) === false)) {
                                    setErrorFullname(true);
                                } else {
                                    setErrorFullname(false);
                                }

                            }}
                            value={fullname}
                            onChangeText={input => setFullname(input)}
                            placeholder={detailData?.data?.fullname} />
                        <Text style={{ marginVertical: 20 }}>Email anda (eg: zero@email.com)</Text>
                        <Input
                            errorMessage={errorEmail ? "format email belum sesuai" : null}
                            onBlur={() => {
                                if ((reg.test(email.trim()) === false)) {
                                    setErrorEmail(true);
                                } else {
                                    setErrorEmail(false);
                                }

                            }}
                            value={email}
                            onChangeText={input => setEmail(input)}
                            placeholder={detailData?.data?.email} />
                        <Text style={{ marginVertical: 20 }}>Password anda (harus berupa huruf dan angka minimal 6 karakter)</Text>
                        <Input
                            value={password}
                            placeholder="Password"
                            errorMessage={errorPassword1 ? "Password harus berupa huruf dan angka minimal 6 karakter" : null}
                            onChangeText={text => setPassword(text)}
                            onBlur={() => {
                                if (password.length < 6 || (letterNumber.test(password) === false)) {
                                    setErrorPassword1(true);
                                } else {
                                    setErrorPassword1(false);
                                }
                            }}
                            rightIcon={
                                <TouchableOpacity
                                    activeOpacity={0.6}
                                    onPress={() => {
                                        setPasswordVisibility(!passwordVisibility)
                                    }}>
                                    <Feather
                                        name={passwordVisibility === true ? "eye-off" : "eye"}
                                        size={moderateScale(22)}
                                        color="#9c9c9c"

                                    />
                                </TouchableOpacity>
                            }
                            secureTextEntry={passwordVisibility}
                        />
                        <Input
                            value={repeatPassword}
                            errorMessage={errorPassword ? "Password tidak sesuai" : null}
                            placeholder="Repeat Password"
                            onChangeText={text => setRepeatPassword(text)}
                            onBlur={() => {
                                if (repeatPassword !== password) {
                                    setErrorPassword(true);
                                } else {
                                    setErrorPassword(false);
                                }

                            }}
                            rightIcon={
                                <TouchableOpacity
                                    activeOpacity={0.6}
                                    onPress={() => {
                                        setRepeatPasswordVisibility(!repeatPasswordVisibility)
                                    }}>
                                    <Feather
                                        name={repeatPasswordVisibility === true ? "eye-off" : "eye"}
                                        size={moderateScale(22)}
                                        color="#9c9c9c"

                                    />
                                </TouchableOpacity>
                            }
                            secureTextEntry={repeatPasswordVisibility}
                        />
                        {detailData.data.gender === 1 ?
                            <Text style={{ marginVertical: 20 }}>Anda seorang : Pria</Text>

                            :

                            <Text style={{ marginVertical: 20 }}>Anda seorang : Wanita</Text>

                        }

                        <Text style={{ marginVertical: 20 }}>Ubah Gender</Text>
                        <RadioButton.Group onValueChange={value => setGender(value)} value={gender}>
                            <RadioButton.Item label="Pria" value={1} />
                            <RadioButton.Item label="Wanita" value={2} />
                        </RadioButton.Group>

                        <Text style={{ marginVertical: 20 }}>Tanggal lahir anda : {detailData.data.dob}</Text>
                        <Text style={{ marginVertical: 20 }}>Ubah tanggal lahir anda</Text>
                        <DatePicker mode="date" date={dob} onDateChange={setDob} />
                        <View style={styles.checkbox}>
                            <CheckBox
                                tintColors={{ true: "#F4AA27", false: "#F4AA27" }}
                                disabled={false}
                                value={toggleCheckBox}
                                onValueChange={newValue => setToggleCheckBox(newValue)}
                            />
                            <Text style={{ fontFamily: 'Quicksand-Bold', color: 'black', fontSize: 14, textAlign: "center", marginTop: 5 }}>I agree with MaimaidApp terms & conditions</Text>

                        </View>
                        <View style={styles.buttonLogin}>
                            <Tombol
                                disabled={!toggleCheckBox
                                    || fullname.length < 3
                                    || (reg.test(email.trim()) === false)
                                    || password !== repeatPassword
                                    || (letters.test(fullname) === false)
                                    || password.length < 6
                                    || (letterNumber.test(password) === false)
                                    || (/\d/.test(password) === false)
                                    || !gender
                                    ? true : false}
                                judul="Edit User"
                                onPress={handleUpdate}
                            />
                            {console.log(letters.test(fullname), "tets")}
                        </View>
                    </View>
                </>

            }

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },

    blueContainer: {
        width: widthPercentageToDP(100),
        height: heightPercentageToDP(18),
        backgroundColor: "#6884F5",
    },

    containContainer: {
        marginTop: moderateScale(5),
        marginHorizontal: moderateScale(20),

    },

    titleStyle: {
        marginTop: moderateScale(15),
        marginLeft: moderateScale(20),

    },

    input: {
        marginTop: moderateScale(10),
        marginHorizontal: moderateScale(20),
    },
    checkbox: {
        marginTop: moderateScale(40),
        flexDirection: 'row',
    },


    nameContainer: {
        fontSize: moderateScale(16),
        borderWidth: moderateScale(1),
        borderRadius: moderateScale(26),
        paddingHorizontal: moderateScale(19),
        paddingVertical: moderateScale(14),
        marginHorizontal: moderateScale(0),
        borderColor: "#d1d2d4",
        fontFamily: "Quicksand-Bold",
        color: "#3E3E3E"
    },




})
