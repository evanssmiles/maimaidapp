import React, { useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ScrollView } from 'react-native'
import axios from 'axios';
import { moderateScale } from 'react-native-size-matters'
//import redux
import { useDispatch, useSelector } from 'react-redux';
import { postHome } from './Redux/action'
import { TouchableOpacity } from 'react-native';

//import vector icons
import Entypo from 'react-native-vector-icons/Entypo'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

//import components
import Loading from '../../Components/Loading'

export default function Home(props) {

    //const
    const dispatch = useDispatch();


    useEffect(() => {
        dispatch(postHome())
    }, [])

    const reload = () => {
        dispatch(postHome())
    }


    //selector
    const listData = useSelector(state => state.HomeReducer?.data?.data?.rows)
    const isLoading = useSelector(state => state.Global?.loading)

    console.log(listData, "ini list data kamu")



    return (
        <SafeAreaView>
            {isLoading ? (
                <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", flex: 1, marginTop: 250 }}>

                    <Loading />
                </View>

            ) : (<>
                {listData === undefined || listData.length < 1 ?
                    null :

                    <>
                        <ScrollView>

                            {listData.map((item, key) => {
                                return (
                                    <TouchableOpacity
                                        onPress={() => props.navigation.navigate('Detail', { id: item.id })}
                                    >
                                        <View style={styles.card} key={key}>
                                            <View style={{ marginLeft: moderateScale(20), marginTop: moderateScale(15) }}>
                                                <Text style={styles.textAlign}>Fullname : {item?.fullname}</Text>
                                                <Text style={styles.textAlign}>Email : {item?.email}</Text>
                                                {item.gender === 1 ?
                                                    <Text style={styles.textAlign}>Gender : Pria</Text>
                                                    :
                                                    <Text style={styles.textAlign}>Gender : Wanita</Text>

                                                }
                                                <Text style={styles.textAlign}>Date Of Birth : {item?.dob}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}

                        </ScrollView>

                    </>
                }
                <View style={{
                    top: moderateScale(580),
                    left: moderateScale(20),
                    position: "absolute",
                    zIndex: 1
                }}>
                    <TouchableOpacity
                        onPress={() => reload()}>
                        <FontAwesome name="refresh" size={50} />
                    </TouchableOpacity>
                </View>



                <View style={{
                    top: moderateScale(580),
                    left: moderateScale(280),
                    position: "absolute",
                    zIndex: 1
                }}>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('Create')}
                    >
                        <Entypo name="add-user" size={50} />
                    </TouchableOpacity>
                </View>


            </>)}







        </SafeAreaView >
    )
}

const styles = StyleSheet.create({

    card: {
        backgroundColor: "white",
        height: moderateScale(150),
        width: moderateScale(300),
        marginTop: moderateScale(20),
        alignSelf: "center"
    },

    textAlign: {
        marginVertical: moderateScale(5),
    }

})
