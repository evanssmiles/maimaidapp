import axios from 'axios';
import { takeLatest, put, select } from 'redux-saga/effects';
import { ToastAndroid } from 'react-native'
import { actionLoading } from '../../../Store/GlobalAction'

//import action
import { setHome } from './action';
import { POST_HOME } from "./action";


function* sagaPostHome(action) {
    try {

        yield put(actionLoading(true))
        const result = yield axios.post(
            `http://maimaid.id:8002/user/read`
        );

        console.log(result, "ini result read");
        if (result.status === 200) {
            // yield put(setUsernameToReducer(result.data));
            yield put(setHome(result.data))
            // ToastAndroid.show('Data found', ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }

    } catch (error) {
        console.log(error.Error);
    } finally {
        yield put(actionLoading(false))
    }
}






function* sagaHome() {
    yield takeLatest(POST_HOME, sagaPostHome)
}

export default sagaHome;
