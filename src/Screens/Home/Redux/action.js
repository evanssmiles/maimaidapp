export const SET_HOME = 'SET_HOME'
export const POST_HOME = "POST_HOME"


export const setHome = payload => {
    return {
        type: SET_HOME,
        payload,
    };
};


export const postHome = payload => {

    return {
        type: POST_HOME,
        payload,
    }
}
