import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CheckBox from '@react-native-community/checkbox'
import { RadioButton } from 'react-native-paper';
import DatePicker from 'react-native-date-picker'
import moment from 'moment'
import md5 from 'md5';
import { Input } from 'react-native-elements'
//import components
import { EmailInput, PasswordInput } from '../../Components/TextInput'
import Tombol from '../../Components/SubmitButton'
import Feather from 'react-native-vector-icons/Feather'
// import { Input } from '../../Components'


//import redux
import { useDispatch, useSelector } from 'react-redux';
import { postCreate } from './Redux/action'
import { postHome } from '../Home/Redux/action'

export default function Create(props) {

    //const
    const [passwordVisibility, setPasswordVisibility] = useState(true);
    const [repeatPasswordVisibility, setRepeatPasswordVisibility] = useState(true);
    const [toggleCheckBox, setToggleCheckBox] = useState(false);
    const [value, setValue] = React.useState('1');
    const [fullname, setFullname] = useState('');
    const [errorFullname, setErrorFullname] = useState(false);
    const [email, setEmail] = useState('');
    const [errorEmail, setErrorEmail] = useState(false);
    const [password, setPassword] = useState('');
    const [repeatPassword, setRepeatPassword] = useState('');
    const [errorPassword, setErrorPassword] = useState(false);
    const [errorPassword1, setErrorPassword1] = useState(false);
    const [gender, setGender] = useState(1);
    const [dob, setDob] = useState(new Date())
    const dispatch = useDispatch();
    const [disable, setDisable] = useState(true);
    const [date, setDate] = useState(new Date())

    //function
    const handleSubmit = () => {
        dispatch(
            postCreate({
                fullname,
                email,
                gender,
                password: md5(password),
                dob: moment(dob).format("YYYY-MM-DD"),
            }),
        );

        props.navigation.navigate("Home")
    };

    //input reformat
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    // const letters = /^[a-zA-Z0-9- ]+$/;
    const letters = /^[a-zA-Z ]*$/
    const letterNumber = /^[0-9a-zA-Z]+$/;



    return (
        <ScrollView style={{ flexGrow: 1, flex: 1 }}>
            <View style={styles.input}>
                <Text style={{ marginVertical: 20 }}>Nama anda (minimal 3 huruf)</Text>
                <Input
                    errorMessage={errorFullname ? "Hanya boleh huruf dan minimal 3 huruf" : null}
                    onBlur={() => {
                        if (fullname.length < 3 || (letters.test(fullname) === false)) {
                            setErrorFullname(true);
                        } else {
                            setErrorFullname(false);
                        }

                    }}
                    value={fullname}
                    onChangeText={input => setFullname(input)}
                    placeholder="Fullname" />
                <Text style={{ marginVertical: 20 }}>Email anda (eg: zero@email.com)</Text>
                <Input
                    errorMessage={errorEmail ? "format email belum sesuai" : null}
                    onBlur={() => {
                        if ((reg.test(email.trim()) === false)) {
                            setErrorEmail(true);
                        } else {
                            setErrorEmail(false);
                        }

                    }}
                    value={email}
                    onChangeText={input => setEmail(input)}
                    placeholder="E-Mail" />
                <Text style={{ marginVertical: 20 }}>Password anda (harus berupa huruf dan angka minimal 6 karakter)</Text>
                {/* <PasswordInput
                    value={password}
                    onChangeText={input => setPassword(input)}
                    placeholder="Password"
                /> */}
                <Input
                    value={password}
                    placeholder="Password"
                    errorMessage={errorPassword1 ? "Password harus berupa huruf dan angka minimal 6 karakter" : null}
                    onChangeText={text => setPassword(text)}
                    onBlur={() => {
                        if (password.length < 6 || (letterNumber.test(password) === false)) {
                            setErrorPassword1(true);
                        } else {
                            setErrorPassword1(false);
                        }

                    }}
                    rightIcon={
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onPress={() => {
                                setPasswordVisibility(!passwordVisibility)
                            }}>
                            <Feather
                                name={passwordVisibility === true ? "eye-off" : "eye"}
                                size={moderateScale(22)}
                                color="#9c9c9c"

                            />
                        </TouchableOpacity>
                    }
                    secureTextEntry={passwordVisibility}
                />
                {/* <PasswordInput
                    value={repeatPassword}
                    onChangeText={input => setRepeatPassword(input)}
                    placeholder="Repeat Password"
                /> */}
                <Input
                    value={repeatPassword}
                    errorMessage={errorPassword ? "Password tidak sesuai" : null}
                    placeholder="Repeat Password"
                    onChangeText={text => setRepeatPassword(text)}
                    onBlur={() => {
                        if (repeatPassword !== password) {
                            setErrorPassword(true);
                        } else {
                            setErrorPassword(false);
                        }

                    }}
                    rightIcon={
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onPress={() => {
                                setRepeatPasswordVisibility(!repeatPasswordVisibility)
                            }}>
                            <Feather
                                name={repeatPasswordVisibility === true ? "eye-off" : "eye"}
                                size={moderateScale(22)}
                                color="#9c9c9c"

                            />
                        </TouchableOpacity>
                    }
                    secureTextEntry={repeatPasswordVisibility}
                />
                <Text style={{ marginVertical: 20 }}>Apakah anda</Text>
                <RadioButton.Group onValueChange={value => setGender(value)} value={gender}>
                    <RadioButton.Item label="Pria" value={1} />
                    <RadioButton.Item label="Wanita" value={2} />
                </RadioButton.Group>

                <Text style={{ marginVertical: 20 }}>Tanggal lahir anda</Text>
                <DatePicker mode="date" date={dob} onDateChange={setDob} />
                <View style={styles.checkbox}>
                    <CheckBox
                        tintColors={{ true: "#F4AA27", false: "#F4AA27" }}
                        disabled={false}
                        value={toggleCheckBox}
                        onValueChange={newValue => setToggleCheckBox(newValue)}
                    />
                    <Text style={{ fontFamily: 'Quicksand-Bold', color: 'black', fontSize: 14, textAlign: "center", marginTop: 5 }}>I agree with MaimaidApp terms & conditions</Text>

                </View>

                <View style={styles.buttonLogin}>
                    <Tombol
                        disabled={!toggleCheckBox
                            || fullname.length < 3
                            || (reg.test(email.trim()) === false)
                            || password !== repeatPassword
                            || (letters.test(fullname) === false)
                            || password.length < 6
                            || (letterNumber.test(password) === false)
                            || (/\d/.test(password) === false)
                            ? true : false}
                        judul="Create User"
                        onPress={handleSubmit}
                    />
                </View>
                {/* {console.log(password.includes(letterNumber), "input")} */}
            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    input: {
        marginTop: moderateScale(10),
        marginHorizontal: moderateScale(20),
    },
    checkbox: {
        marginTop: moderateScale(40),
        flexDirection: 'row',
    },
    buttonLogin: {
        alignItems: 'center',
        marginTop: moderateScale(20),

    },

})
