export const SET_CREATE = 'SET_CREATE'
export const POST_CREATE = "POST_CREATE"


export const setCreate = payload => {
    return {
        type: SET_CREATE,
        payload,
    };
};


export const postCreate = payload => {
    return {
        type: POST_CREATE,
        payload,
    }
}
