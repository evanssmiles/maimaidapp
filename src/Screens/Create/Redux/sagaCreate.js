import axios from 'axios';
import { takeLatest, put, select } from 'redux-saga/effects';
import { ToastAndroid } from 'react-native'
import { actionLoading } from '../../../Store/GlobalAction'

//import action
import { setDetail } from './action';
import { POST_CREATE } from "./action";


function* sagaPostCreate(action) {
    try {
        const result = yield axios.post(
            `http://maimaid.id:8002/user/create`,
            action.payload,
        );

        console.log(result, "ini result create");

        if (result.status === 200) {
            // yield put(setUsernameToReducer(result.data));
            ToastAndroid.show('Data berhasil ditambahkan', ToastAndroid.LONG, ToastAndroid.BOTTOM);
        }

    } catch (err) {
        console.log(err);
        ToastAndroid.show('Email sudah terdaftar', ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
}






function* sagaCreate() {
    yield takeLatest(POST_CREATE, sagaPostCreate)
}

export default sagaCreate;
