import 'react-native-gesture-handler';
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

//import stack root
import Root from './Root'


//import Redux & Store
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react'
import { Store, Persistor } from './src/Store/Store'

export default function App() {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <Root />
      </PersistGate>
    </Provider>
  )
}

const styles = StyleSheet.create({})
